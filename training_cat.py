import json
import os
import pickle
from datetime import datetime

import h5py as h5
import keras
import numpy as np
from keras.layers import LSTM, Activation, Dense, Dropout
from keras.utils.np_utils import to_categorical
from keras.models import Sequential, load_model
from tqdm import tqdm

from crypto import CryptoPair
from ms.data import Data
from ms import lstm_cat


def main():
    with open('./config.json') as json_data:
        config = json.load(json_data)
        json_data.close()

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    pair = poloniex_pairs[0]
    crypto_name = pair.escape()
    data = Data(interval='15min',
                crypto=pair, window_size=100, name=crypto_name)

    training_labels, training_features = data.training()
    validation_labels, validation_features = data.validation()

    training_labels = training_labels + 1
    validation_labels = validation_labels + 1
    training_labels_cat = to_categorical(training_labels, num_classes=13)
    validation_labels_cat = to_categorical(validation_labels, num_classes=13)

    path = './models/lstm_cat/'
    filename = path + '{}-15min_lstm_cat.model'.format(pair.escape())
    history_filename = path + 'lstm_cat_{}.history'\
        .format(datetime.now().strftime('%y-%m-%M_%H-%S'))

    model: Sequential
    if os.path.exists(filename) and os.path.isfile(filename):
        model: Sequential = load_model(filename)
    else:
        model: Sequential = lstm_cat.build_model(training_features)

    result = model.fit(x=training_features, y=training_labels_cat,
                       epochs=5, batch_size=256,
                       verbose=1,
                       validation_data=(
                           validation_features,
                           validation_labels_cat
                       ),
                       shuffle=False)

    model.save(filename)


main()
