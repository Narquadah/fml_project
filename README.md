# Nostradamus
* Nicholas Bade
* Kim Almasan

## Dependencies
* Python 3.6
* Pipenv
    * `(sudo) pip3 install pipenv`

# Data Fetching

## Run
1. `pipenv install`
2. `pipenv shell`
3. `python data_fetcher_averages.py` or `python data_fetcher_exchanges.py`


## Data files
* `db/cryptos_data.db`
    * An sqlite database with 5 minute intervals of crypto history from Binance and Poloniex
* `db/current_data.db`
    * An sqlite database with 5 minutes intervals of all cryptocurrencies listed on [coinmarketcap](https://coinmarketcap.com)
    * Has no historical data and uses averages from multiple exchanges.
* `db/ms_data_*`
    * Files named like this are processed cryptocurrency courses for fitting models
* `db/current_data_*`
    * Processed data from coinmarketcap for prediction and comparison


# Data Preparation
## Run
1. `pipenv install`
2. `pipenv shell`
3. `python prepare_data.py` or `python prepare_average_data.py`


# Data Plots
## Setup
Uncomment one or more of the following lines in the file `visualize.py` in order to select which type of plot should be generated.
```py
# Careful it takes a few hours
start_scatter_plots(poloniex, poloniex_pairs)

start_normal_plot(poloniex, poloniex_pairs)

plot_covariance(poloniex, poloniex_pairs)
```
## Run
1. `pipenv install`
2. `pipenv shell`
3. `python visualize.py`

# Training
The Training can be modified in the file `training.py`. The models can be found in the `ms` folder.
The `data.py` loads the data and formats the sliding window and which data from the prepared data is selected.
```py
data = Data(interval,
            crypto,
            window_size,
            prefix='',
            name='',
            batch_size=1,
            label_size=1,
            filename='ms',
            min_max_feature_range=(-1, 1)):
```

In order to select the model edit the variable `SELECTED_MODEL` with the models `lstm_1`, `lstm_2`, or `lstm_3`.
To edit the models open one of the files `lstm_1.py`, `lstm_2.py`, or `lstm_3.py` in the `ms` folder.

## Run
1. `pipenv install`
2. `pipenv shell`
3. `python training.py`

# Prediction
Predictions can be found in the `models` folders formatted in `MODEL_NAME_RUN`. In each of the runs there are checkpoints of each of epochs with loss, the second folder is the tensorboard which can be started with `pipenv run tensorboard --logdir ./models/lstm_1_run_1/tensorboard`. The `info.txt` file has basic information/configuration of the run. The `csv` file contains the training history of all epochs. Furthermore, the `prediction.py` predicts the future of a quotation for the next 100 intervals and compares it with the real data.

## Run
1. `pipenv install`
2. `pipenv shell`
3. `python prediction.py`

## Visualizing prediction history
Plots the prediction history of all runs into the `plots` folder.

### Run
1. `pipenv install`
2. `pipenv shell`
3. `python visualize_history.py`
