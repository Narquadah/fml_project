""" The Market Association queries the data source for retired market data  """

import json
from typing import List

import h5py as h5
import numpy as np
import pandas as pd
from pandas import io
from tqdm import tqdm

from crypto import CryptoPair
from persistance import Database


def get_current(pair):
    """ Associates a pair from the database to a dataframe """
    Database.DATABASE_URL = 'sqlite:///db/current_data.db'
    Database.initialize()
    query = """
    SELECT * FROM coinmarketcap WHERE symbol = '{}'
    ORDER BY last_updated DESC
    """.format(pair.base)
    read = io.sql.read_sql_query(query, Database.engine)\
        .sort_values(by="last_updated")\
        .drop_duplicates(subset='last_updated')[
            ['price_usd', 'price_btc', 'last_updated']]
    return read


def simple_merge(data: pd.DataFrame):
    data = data.reset_index()
    if len(data) == 0:
        return 0
    res = (data.price_btc[len(data) - 1] / data.price_btc[0]) * 100

    if res < -0.3:
        return -1
    elif res <= -0.5 and res > -0.7:
        return -2
    elif res <= -0.7 and res > -1.0:
        return -3
    elif res <= -1.0 and res > -2.0:
        return -4
    elif res <= -2.0 and res > -5.0:
        return -5
    elif res <= -5:
        return -6
    elif res >= 0.3 and res < 0.5:
        return 1
    elif res >= 0.5 and res < 0.7:
        return 2
    elif res >= 0.7 and res < 1.0:
        return 3
    elif res >= 1.0 and res < 2.0:
        return 4
    elif res >= 2.0 and res < 5.0:
        return 5
    elif res >= 5:
        return 6

    return 0


def percentage_positive_merge(data: pd.DataFrame):
    if len(data) == 0:
        return 0
    data = data.reset_index()
    res = (data.price_btc[len(data) - 1] / data.price_btc[0]) * 100
    return res


def percentage_merge(data: pd.DataFrame):
    if len(data) == 0:
        return 0
    data = data.reset_index()
    res = ((data.price_btc[len(data) - 1] / data.price_btc[0]) - 1) * 100
    return res


def closing_merge(data: pd.DataFrame):
    if len(data) == 0:
        return 0
    data = data.reset_index()
    res = data.price_btc[len(data) - 1]
    return res


def convert_currency(df: pd.DataFrame, timeframe, merge):
    df = df.reset_index()
    df.last_updated = pd.to_datetime(df.last_updated, unit='s')
    grouped_data = df.groupby(pd.Grouper(key='last_updated', freq=timeframe))\
        .apply(merge)

    labels = np.array([
        int(timestamp.value * 10 ** -9)
        for timestamp in grouped_data.keys().tolist()
    ])

    features = np.array(grouped_data)
    return labels, features


def convert(ohlcv, timeframe='15min', merge=simple_merge):
    return {
        key: convert_currency(data, timeframe, merge=merge)
        for key, data in ohlcv.items()
    }


def main():
    with open('./config.json') as json_data:
        config = json.load(json_data)
        json_data.close()

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    timeframes = [
        '15min',
        # '1H',
        # '4H',
        # '12H',
        # '1D',
        # '1W',
        # '1M',
    ]

    merge_functions = [
        simple_merge,
        percentage_merge,
        percentage_positive_merge,
        closing_merge
    ]

    filtered_pairs = filter(lambda itm: itm.quote == 'BTC', poloniex_pairs)
    current_data = {pair(): get_current(pair) for pair in filtered_pairs}

    for merge in tqdm(merge_functions):
        print('Evaluating: ' + merge.__name__)
        print('convertig candlestick data')
        data_to_save = {
            key: convert(current_data, timeframe=key, merge=merge)
            for key in timeframes
        }

        for key, prepared_data in data_to_save.items():
            filename = './db/current_data_{}_{}.h5'\
                .format(merge.__name__.replace('_merge', ''), key)

            with h5.File(filename, 'w') as file:
                for crypto, data in prepared_data.items():
                    c = file.create_group(crypto.replace('/', '-'))
                    c.create_dataset('labels', data=data[0])
                    c.create_dataset('features', data=data[1])


main()
