""" Main file to start the file, entrypoint! """
# import logging
# logging.basicConfig(level=logging.DEBUG)

import asyncio
import datetime
import itertools
import json
import math
import os
from datetime import datetime
from typing import Dict, List

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm

from crypto.currency import CryptoPair
from crypto.exchange import Binance, Exchange, Poloniex
from persistance import Association, Retirement


def simple_merge(data: pd.DataFrame):
    data = data.reset_index()
    res = (data.Close[len(data) - 1] / data.Open[0]) * 100

    if res < -0.3:
        return -1
    elif res <= -0.5 and res > -0.7:
        return -2
    elif res <= -0.7 and res > -1.0:
        return -3
    elif res <= -1.0 and res > -2.0:
        return -4
    elif res <= -2.0 and res > -5.0:
        return -5
    elif res <= -5:
        return -6
    elif res >= 0.3 and res < 0.5:
        return 1
    elif res >= 0.5 and res < 0.7:
        return 2
    elif res >= 0.7 and res < 1.0:
        return 3
    elif res >= 1.0 and res < 2.0:
        return 4
    elif res >= 2.0 and res < 5.0:
        return 5
    elif res >= 5:
        return 6

    return 0


def percentage_positive_merge(data: pd.DataFrame):
    data = data.reset_index()
    res = (data.Close[len(data) - 1] / data.Open[0]) * 100
    return res


def percentage_merge(data: pd.DataFrame):
    data = data.reset_index()
    res = ((data.Close[len(data) - 1] / data.Open[0]) - 1) * 100
    return res


def closing_merge(data: pd.DataFrame):
    data = data.reset_index()
    res = data.Close[len(data) - 1]
    return res


def convert_currency(df: pd.DataFrame, timeframe, merge):
    df = df.reset_index()
    df.Timestamp = pd.to_datetime(df.Timestamp, unit='ms')
    grouped_data = df.groupby(pd.Grouper(key='Timestamp', freq=timeframe))\
        .apply(merge)

    labels = np.array([
        int(timestamp.value * 10 ** -9)
        for timestamp in grouped_data.keys().tolist()
    ])

    features = np.array(grouped_data)
    return labels, features


def convert(ohlcv, timeframe='15min', merge=simple_merge):
    return {
        key: convert_currency(data, timeframe, merge=merge)
        for key, data in ohlcv.items()
    }


def convert_to_usd_base(ohlcv: Dict[str, pd.DataFrame],
                        pairs: List[CryptoPair]):

    btc_base = ohlcv['BTC/USDT'][['Timestamp', 'Close', 'Open']]

    usd = {
        'BTC/USD': btc_base
    }

    for pair in pairs:
        if pair.quote == 'BTC':
            current = ohlcv[pair()][['Timestamp', 'Close', 'Open']].loc[:]
            current.loc[:].Close = current.loc[:].Close * btc_base.loc[:].Close
            current.loc[:].Open = current.loc[:].Open * btc_base.loc[:].Open
            usd[pair.base + '/USD'] = current
    return usd


def main():
    """ Starts the main program and handles the cli api """
    with open('./config.json') as json_data:
        config = json.load(json_data)
        json_data.close()

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    # 105192 5 minutes in a year
    # 8766 5 minutes in a month
    # 525960 minutes in year
    Retirement.Cap = 400_000
    Association.Cap = 400_000

    # binance = Binance(cryptos=binance_pairs)
    poloniex = Poloniex(cryptos=poloniex_pairs)
    timeframes = [
        '15min',
        # '1H',
        # '4H',
        # '12H',
        # '1D',
        # '1W',
        # '1M',
    ]

    merge_functions = [
        simple_merge,
        percentage_merge,
        percentage_positive_merge,
        closing_merge
    ]

    for merge in tqdm(merge_functions):
        print('Evaluating: ' + merge.__name__)
        print('convertig candlestick data')
        data_to_save = {
            key: convert(poloniex.market.ohlcv, timeframe=key, merge=merge)
            for key in timeframes
        }

        print('convert to us dollar')
        usd_data = convert_to_usd_base(poloniex.market.ohlcv, poloniex_pairs)

        usd_data_to_save = {
            key: convert(usd_data, timeframe=key, merge=merge)
            for key in timeframes
        }

        print('saving btc base data')
        for key, prepared_data in data_to_save.items():
            filename = './db/ms_data_{}_{}.h5'\
                .format(merge.__name__.replace('_merge', ''), key)

            with h5.File(filename, 'w') as file:
                for crypto, data in prepared_data.items():
                    c = file.create_group(crypto.replace('/', '-'))
                    c.create_dataset('labels', data=data[0])
                    c.create_dataset('features', data=data[1])

        print('saving dollar base data')
        for key, prepared_data in usd_data_to_save.items():
            filename = './db/ms_data_usd_{}_{}.h5'\
                .format(merge.__name__.replace('_merge', ''), key)

            with h5.File(filename, 'w') as file:
                for crypto, data in prepared_data.items():
                    c = file.create_group(crypto.replace('/', '-'))
                    c.create_dataset('labels', data=data[0])
                    c.create_dataset('features', data=data[1])


main()
