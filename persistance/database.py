""" Manages Database Connection """

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Database():
    """
    Manages Database Connection
    """

    DATABASE_URL = 'sqlite:///db/cryptos_data.db'
    Base = declarative_base()
    # engine = create_engine(DATABASE_URL, echo="debug")
    engine = create_engine(DATABASE_URL)
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    is_initialized = False

    @staticmethod
    def initialize():
        """ Initializes the database if it isn't already """
        if not Database.is_initialized:
            Database.engine = create_engine(Database.DATABASE_URL)
            Database.DBSession = sessionmaker(bind=Database.engine)
            Database.session = Database.DBSession()
            Database.Base.metadata.create_all(Database.engine)
            Database.is_initialized = True
