""" The Market Association queries the data source for retired market data  """

from typing import List

from pandas import io

from crypto.currency import CryptoPair
from .database import Database


class Association():
    """
    The Market Association queries the
    data source for retired market data
    """

    Cap = 60

    def __init__(self, market):
        self.market = market

    def __call__(self):
        pass


class SQLiteAssociation(Association):
    """
    SQLite Association
    """

    def __init__(self, market, pairs: List[CryptoPair]):
        super().__init__(market)
        Database.initialize()
        self.pairs = pairs

    def __call__(self):
        return {
            pair(): self.associate(pair()) for pair in self.pairs
        }

    def associate(self, pair: CryptoPair):
        """ Associates a pair from the database to a dataframe """
        query = """
        SELECT * FROM '{}' WHERE Exchange = '{}'
        ORDER BY Timestamp DESC LIMIT {}
        """.format(pair, self.market.name, self.Cap)
        try:
            read = io.sql.read_sql_query(query, Database.engine)\
                .sort_values(by="Timestamp")
            return read
        except Exception:
            return None
