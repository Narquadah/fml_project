""" Retires old date from memory to a database """


from pandas import DataFrame

from .database import Database


class Retirement():
    """
    Retires old date from memory to a database
    """
    Cap = 60

    def __init__(self, market):
        self.market = market

    def __call__(self):
        pass


class SQLiteRetirement(Retirement):
    """
    SQLite Retirement
    """

    def __init__(self, market):
        super().__init__(market)
        Database.initialize()
        self.session = Database.session

    def __call__(self):
        if self.market.ohlcv is None:
            return

        self.market.ohlcv = {
            key: self.retire(key=key, price_collection=df)
            for key, df in self.market.ohlcv.items()
        }

    def retire(self, price_collection: DataFrame, key: str):
        """ Reties a certain amount of Rows """
        if price_collection is None:
            return None

        if len(price_collection) < self.Cap:
            return price_collection
        to_save: DataFrame = price_collection[:]  # [:-self.cap]
        to_save = to_save.drop(to_save[to_save['saved'] == 1].index)
        to_save = to_save.replace(to_replace={'saved': False}, value=True)

        tablename = key
        if len(to_save) > 0:
            to_save.to_sql(name=tablename,
                           con=Database.engine,
                           if_exists='append',
                           index=False)

        rtn_df = price_collection[-self.cap:]\
            .replace(to_replace={'saved': False}, value=True)

        return rtn_df
