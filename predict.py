import json
import os
import pickle
import sys
from datetime import datetime

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential, load_model
from pandas import DataFrame
from sklearn.metrics import mean_squared_error
from tqdm import tqdm

from crypto import CryptoPair
from ms.data import Data


def predict_loop(model, start):

    predicted_list = []
    for i in range(start.shape[1]):
        result = model.predict(start, verbose=0)
        predicted_list.append(result)
        # print(result)
        # start = np.array([*start[len(result):], result])
        start = np.array([*np.squeeze(start)[1:], np.squeeze(result)])
        start = start.reshape((1, len(start), 1))
    return np.squeeze(np.array(predicted_list))


def transform(data_current, value):
    return data_current.scaler.inverse_transform(value)


def merge_data(predicted, real):
    data = [[itm_1, itm_2] for itm_1, itm_2 in zip(predicted, real)]
    df = DataFrame(data=data, columns=['Predicted', 'Real'])
    return df


def compare_to_real_data(predicted, real, data):
    if data:
        return [
            [start, mean_squared_error(
                transform(data, predicted[0:start]),
                transform(data, real[0:start]))]
            for start in range(len(predicted))
        ]
    else:
        return [
            [start, mean_squared_error(
                data, predicted[0:start],
                data, real[0:start])]
            for start in range(len(predicted))
        ]


def predict(model: Sequential, labels, features):
    print('predicting....')
    pre = model.predict(features)
    np.round(pre)
    print(pre)


def plot(df: DataFrame):
    plt.figure(1)
    df.plot()
    plt.ylabel('Percentage')
    plt.xlabel('15min Interval')
    plt.show()
    plt.close()


def main():
    past_learned = len(sys.argv) == 2
    window_size = 100
    scaler = False
    model_path = 'models/lstm_2_run_5/checkpoints/lstm_2-38-98.99309.model'

    with open('./config.json') as json_data:
        config = json.load(json_data)

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    pair = poloniex_pairs[0]
    crypto_name = pair.escape()

    data_current = Data(interval='percentage_positive_15min',
                        crypto=pair,
                        window_size=window_size,
                        name=crypto_name,
                        batch_size=16,
                        label_size=1,
                        filename='current',
                        enable_scaler=scaler)

    data_training = Data(interval='percentage_positive_15min',
                         crypto=pair,
                         window_size=window_size,
                         name=crypto_name,
                         batch_size=16,
                         label_size=1,
                         enable_scaler=scaler)

    files = os.popen('find models -type f -name "*.model"')\
        .read()\
        .split('\n')[0:-1]

    predictions = []

    data = data_current.data

    training_labels, training_features = data_training.training()
    validation_labels, validation_features = data_training.validation()

    # path = 'db/percent_current_data_15min.h5'
    # with h5.File(path, mode='r') as file:
    #     data = np.array(file['ETH-BTC']['features'])

    start = data[0:window_size].reshape((1, window_size, 1))
    real = data[window_size:window_size * 2]
    model: Sequential = load_model(model_path)

    if past_learned:
        past_predicted = model.predict(training_features[0:window_size])
        real_labels = training_labels[0:window_size]

        if scaler:
            past_predicted = transform(data_training, past_predicted)
            real_labels = np.squeeze(
                transform(data_training, np.array(real_labels).reshape(-1, 1)))

        past = merge_data(np.squeeze(past_predicted),
                          real_labels)

        print('Plotting Past Data')

        plot(past)
    else:
        result = predict_loop(model, start)

        if scaler:
            result = np.squeeze(
                transform(data_training, result.reshape(-1, 1)))
            real = np.squeeze(transform(data_current, real.reshape(-1, 1)))

        df = merge_data(result, real)
        print('Plotting Current Data')
        plot(df)
        print(df)

    # comparison = compare_to_real_data(result, real, data_current)
    # comparison = compare_to_real_data(result, real)


main()
