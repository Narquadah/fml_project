""" Main file to start the file, entrypoint! """
# import logging
# logging.basicConfig(level=logging.DEBUG)

import asyncio
import datetime
import itertools
import json
import math
import os
from datetime import datetime
from typing import List, Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.offline as py
import seaborn as sns

from crypto.currency import CryptoPair
from crypto.exchange import Binance, Exchange, Poloniex
from persistance import Association, Retirement


def make_dates(timestamps, chunk_size):
    timestamps_order = np.empty(len(timestamps))
    timestamps_order[0:len(timestamps)] = timestamps[::-1]
    dates = []
    for i in range(0, len(timestamps), chunk_size):
        dates.append(datetime.fromtimestamp(timestamps_order[i] / 1000))

    return dates


def make_slices(close, chunk_size: int):
    amount_of_intervals = math.ceil(len(close) / chunk_size)
    empty_array = np.zeros(chunk_size * amount_of_intervals)

    empty_array[0:len(close)] = np.array(close)

    reshaped_data = np.reshape(
        empty_array, (amount_of_intervals, chunk_size))
    return reshaped_data


def average(df):
    return [np.average(df.Close[i:i + 288]) for i in range(0, len(df), 288)]


def percentage(df):
    length = (len(df) % 288) * 288
    return [(df.Close[i + 288] / df.Close[i]) * 100
            for i in range(0, length, 288)]


def plots_scatter(first, second, path, filename, range_name, title):
    if not os.path.exists(path):
        os.makedirs(path)

    temp_range_first = range(0, len(first))
    temp_range_second = range(0, len(second))
    for x, y in itertools.product(temp_range_first, temp_range_second):
        plt.figure(1)
        plt.title(title)
        plt.xlabel(range_name + ': ' + str(x + 1))
        plt.ylabel(range_name + ': ' + str(y + 1))
        plt.scatter(first[x], second[y])
        plt.savefig(filename.replace('sub1', str(
            x + 1)).replace('sub2', str(y + 1)))
        plt.close()


def visualize_scatter_plots(exchange: Exchange,
                            pair,
                            other_pair,
                            chunk_size: int,
                            range_name: str):

    df: pd.DataFrame = exchange.market\
        .ohlcv[pair()][['Timestamp', 'Close']][::-1]

    df_other: pd.DataFrame = exchange.market\
        .ohlcv[other_pair()][['Timestamp', 'Close']][::-1]

    first_literal = make_slices(average(df), chunk_size)
    second_literal = make_slices(average(df_other), chunk_size)

    first_percentage = make_slices(percentage(df), chunk_size)
    second_percentage = make_slices(percentage(df_other), chunk_size)

    path_literal = 'plots/comparison/scatter_plots/literal/{}/{}_{}/'.format(
        range_name, pair().replace('/', '-'), other_pair().replace('/', '-'))
    filename_literal = '{}scatter_{}_{}_sub1-sub2.png'\
        .format(path_literal,
                pair().replace('/', '-'),
                other_pair().replace('/', '-'))

    path_percentage = 'plots/comparison/scatter_plots/percentage/{}/{}_{}/'\
        .format(range_name,
                pair().replace('/', '-'),
                other_pair().replace('/', '-'))

    filename_percentage = '{}percentage_{}_{}_sub1-sub2.png'\
        .format(path_percentage,
                pair().replace('/', '-'),
                other_pair().replace('/', '-'))

    plots_scatter(first_literal,
                  second_literal,
                  path_literal,
                  filename_literal,
                  range_name,
                  title='{} vs {}'.format(pair(), other_pair())
                  )

    plots_scatter(first_percentage,
                  second_percentage,
                  path_percentage,
                  filename_percentage,
                  range_name,
                  title='{} vs {}'.format(pair(), other_pair())
                  )


def start_scatter_plots(poloniex, poloniex_pairs):
    for first, second in itertools.product(poloniex_pairs, poloniex_pairs):
        print('Running: {} vs. {}'.format(first(), second()))
        visualize_scatter_plots(
            poloniex,
            pair=first,
            other_pair=second,
            chunk_size=30,
            range_name='months')

        visualize_scatter_plots(
            poloniex,
            pair=first,
            other_pair=second,
            chunk_size=360,
            range_name='years')


def visualize_normal_plot(exchange, currency):
    currency = currency()
    df: pd.DataFrame = exchange.market\
        .ohlcv[currency][['Timestamp', 'Close']][::-1]

    df.Timestamp = pd.to_datetime(df.Timestamp, unit='ms')

    # data = [go.Scatter(x=df['Timestamp'], y=np.log10(df['Close'])])
    data = [go.Scatter(x=df['Timestamp'], y=df['Close'])]
    layout = go.Layout(
        title=currency,
        xaxis={
            'title': 'Time'
        },
        yaxis={
            # 'title': 'log10'
            'title': 'BTC'
        }
    )
    fig = go.Figure(data=data, layout=layout)

    path = 'plots/overview/'
    if not os.path.exists(path):
        os.makedirs(path)
    py.plot(fig, filename='{}{}.html'.format(path, currency.replace('/', '-')))


def start_normal_plot(poloniex, poloniex_pairs):
    for idx, currency in enumerate(poloniex_pairs):
        print("{} / {} - Plotting: '{}'"
              .format(idx + 1, len(poloniex_pairs), currency()))
        visualize_normal_plot(poloniex, currency)


def translate_btc_to_usd(ohlcv: Dict[str, pd.DataFrame],
                         pairs: List[CryptoPair]):

    btc_base = ohlcv['BTC/USDT'][['Timestamp', 'Close']]\
        .rename(columns={'Close': 'BTC'})

    joined: pd.DataFrame = btc_base.set_index('Timestamp')

    for pair in pairs:
        if pair.quote == 'BTC':
            current = ohlcv[pair()][['Timestamp', 'Close']]
            current.loc[:].Close = current.loc[:].Close * btc_base.loc[:].BTC
            current = current.rename(columns={'Close': pair.base})
            joined = joined.join(current.set_index('Timestamp'))
    return joined


def plot_covariance(poloniex, poloniex_pairs):
    data: pd.DataFrame = translate_btc_to_usd(poloniex.market.ohlcv,
                                              poloniex_pairs)

    overall = data.corr()
    year = data[0:210_384].corr(min_periods=105_192)
    ax = sns.heatmap(year, annot=True, fmt=".2f")
    ax.figure.savefig('plots/overall_correlation.png')
    plt.close()

    ax = sns.heatmap(overall, annot=True, fmt=".2f")
    ax.figure.savefig('plots/2016_2017_correlation.png')
    plt.close()


def main():
    """ Starts the main program and handles the cli api """
    with open('./config.json') as json_data:
        config = json.load(json_data)
        json_data.close()

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    # 105192 5 minutes in a year
    # 8766 5 minutes in a month
    # 525960 minutes in year
    Retirement.Cap = 400_000
    Association.Cap = 400_000

    # binance = Binance(cryptos=binance_pairs)
    poloniex = Poloniex(cryptos=poloniex_pairs)

    # Careful it takes a few hours
    # start_scatter_plots(poloniex, poloniex_pairs)

    # start_normal_plot(poloniex, poloniex_pairs)

    plot_covariance(poloniex, poloniex_pairs)


main()
