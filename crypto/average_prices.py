"""

"""

import json
import requests
from decimal import Decimal


from sqlalchemy import Column, Integer, String, Numeric

from persistance import Database

COINMARKETCAP_URL = 'https://api.coinmarketcap.com/v1/ticker/'


class AveragePrices():
    """

    """

    def __init__(self, providers=None):
        self.providers = providers if providers else [CoinMarketCap]

    def run(self):
        Database.initialize()
        for provider in self.providers:
            data = provider.fetch()
            Database.session.add_all(data)
            Database.session.commit()

    async def start_get_interval(self):
        pass


class CoinMarketCap(Database.Base):
    """

    """

    __tablename__ = 'coinmarketcap'

    id = Column(Integer, primary_key=True)
    str_id = Column(String)
    name = Column(String)
    symbol = Column(String)
    rank = Column(Integer)
    price_usd = Column(Numeric)
    price_btc = Column(Numeric)
    twenty_four_h_volume_usd = Column(Numeric)
    market_cap_usd = Column(Numeric)
    available_supply = Column(Numeric)
    total_supply = Column(Numeric)
    max_supply = Column(Numeric, nullable=True)
    percent_change_1h = Column(Numeric)
    percent_change_24h = Column(Numeric)
    percent_change_7d = Column(Numeric)
    last_updated = Column(String)

    def __init__(self, json_object):
        self.parse(json_object)

    def parse(self, json_object):
        self.str_id = json_object['id']
        self.name = json_object['name']
        self.symbol = json_object['symbol']
        self.rank = int(json_object['rank'] if json_object['rank'] else 0)
        self.price_usd = Decimal(
            json_object['price_usd'] if json_object['price_usd'] else 0)
        self.price_btc = Decimal(
            json_object['price_btc'] if json_object['price_btc'] else 0)
        self.twenty_four_h_volume_usd = Decimal(
            json_object['24h_volume_usd'] if json_object['24h_volume_usd'] else 0)
        self.market_cap_usd = Decimal(
            json_object['market_cap_usd'] if json_object['market_cap_usd'] else 0)
        self.available_supply = Decimal(
            json_object['available_supply'] if json_object['available_supply'] else 0)
        self.total_supply = Decimal(
            json_object['total_supply'] if json_object['total_supply'] else 0)

        self.max_supply = Decimal(
            json_object['max_supply'] if json_object['max_supply'] else 0)

        self.percent_change_1h = Decimal(
            json_object['percent_change_1h'] if json_object['percent_change_1h'] else 0)
        self.percent_change_24h = Decimal(
            json_object['percent_change_24h'] if json_object['percent_change_24h'] else 0)
        self.percent_change_7d = Decimal(
            json_object['percent_change_7d'] if json_object['percent_change_7d'] else 0)
        self.last_updated = int(
            json_object['last_updated'] if json_object['last_updated'] else 0)

    @staticmethod
    def fetch(limit=0):
        req = requests.get(COINMARKETCAP_URL, params={'limit': limit})
        json_objects = req.json()
        return [CoinMarketCap(json_object) for json_object in json_objects]
