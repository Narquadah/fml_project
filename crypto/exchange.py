""" Exchanges avaliable """

from abc import ABC
from typing import Union, List
# TODO Add async
from time import sleep, time
from ccxt import Exchange as imported_exchange
from ccxt import binance, poloniex

from persistance import SQLiteAssociation, SQLiteRetirement
from .market import Market
from .currency import CryptoPair


class Exchange(ABC):
    """ Abstract Base Class for Exchanges """

    def __init__(self,
                 exchange,
                 cryptos: List[CryptoPair],
                 api_key: str,
                 secret: str = None,
                 uid: Union[str, int, None]=None,
                 password: str = None):

        self.exchange: imported_exchange = exchange(config={
            'apiKey': api_key,
            'secret': secret,
            'uid': uid,
            'password': password
        })

        self.timeout = 0.5
        self.timeframe = '1m'
        self.timestamp_factor = 1
        self.timeframe_in_minutes = 1

        self.check_and_set_cryptos(cryptos)
        self.market = Market(name=self.exchange.name,
                             pairs=self.cryptos,
                             retirement=SQLiteRetirement,
                             association=SQLiteAssociation)

        if api_key or secret or uid or password:
            self.exchange.check_required_credentials()

    def check_and_set_cryptos(self, cryptos: List[CryptoPair]):
        """
        Checks whether a list of cryptopairs
        is available in the exchange and sets it
        """
        self.cryptos = cryptos

        if not self.cryptos:
            return

        for pair in self.cryptos:
            if not self.is_available_crypto(pair=pair):
                raise ValueError("CryptoPair '{}' is not available in '{}'"
                                 .format(pair, self.exchange.name))

    def is_available_crypto(self, pair: CryptoPair):
        """ Check whether a crypto pair is available """
        self.exchange.load_markets()
        return pair() in self.exchange.markets

    async def collect(self):
        """ Collects the current market conditions from a list of cryptos """
        for pair in self.cryptos:
            entry = await self.get_current_price(pair)
            self.market.add(pair=pair,
                            entry=entry,
                            further_data=[self.timeframe, self.timestamp_factor])

    async def get_current_price(self, pair: CryptoPair):
        """ Gets the current ticker/ohlcv price for a crypto pair """
        if self.exchange.has['fetchOHLCV']:
            return self.get_current_price_ohlcv(pair)
        elif self.exchange.has['fetchTicker']:
            return self.get_current_price_ticker(pair)

        raise AttributeError('{} has neither fetch ticker nor ohlcv'
                             .format(self.exchange.name))

    def get_current_price_ticker(self, pair: CryptoPair):
        """ Gets the current ticker price for a crypto pair """
        raise NotImplementedError('Ticker Prices are not yet implemented')

    def get_current_price_ohlcv(self, pair: CryptoPair):
        """ Gets the current ohlcv price for a crypto pair """
        fetched_data = self.exchange\
            .fetch_ohlcv(symbol=pair(),
                         since=self.market.get_last_fetch_timestamp(pair) * self.timestamp_factor,
                         timeframe=self.timeframe)
        sleep(self.exchange.rateLimit / 1000)

        return fetched_data

    def check_time_is_current(self):
        """ Checks whether the collected data is current """
        not_current_cryptos = []
        for crypto in self.cryptos:
            is_current = self.market\
                .get_last_fetch_timestamp(crypto) > ((time() - self.timeframe_in_minutes * 60) * 1000)
            if not is_current:
                not_current_cryptos.append(crypto)

        return not_current_cryptos

    async def catch_up(self):
        """ Catch up to current data """
        while True:
            not_current_cryptos = self.check_time_is_current()
            if len(not_current_cryptos) == 0:
                break
            for crypto in not_current_cryptos:
                entry = await self.get_current_price(crypto)
                self.market.add(pair=crypto,
                                entry=entry,
                                further_data=[self.timeframe, self.timestamp_factor])


    # TODO remove me
    def debug_exchange(self):
        pass


class Binance(Exchange):
    """ Binance Exchange """

    def __init__(self,
                 cryptos: List[CryptoPair]=None,
                 api_key: str=None,
                 secret: str=None):

        super().__init__(cryptos=cryptos,
                         exchange=binance,
                         api_key=api_key,
                         secret=secret)

        # self.timestamp_factor = 1000


class Poloniex(Exchange):
    """ Poloniex Exchange """

    def __init__(self,
                 cryptos: List[CryptoPair]=None,
                 api_key: str=None,
                 secret: str=None):

        super().__init__(cryptos=cryptos,
                         exchange=poloniex,
                         api_key=api_key,
                         secret=secret)

        self.timeframe = '5m'
        self.timeframe_in_minutes = 5