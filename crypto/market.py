"""
Contains the current market conditions for
multiple crypto pairs for a single exchange
"""

from typing import Dict, List

from pandas import DataFrame

from persistance import Association, Retirement
from .currency import CryptoPair

OHLCV_COLUMN_NAMES = [
    "Timestamp",
    "Open",
    "Highest",
    "Lowest",
    "Close",
    "Volume",
    "CryptoPair",
    "Exchange",
    "Timeframe",
    "Timestamp_Factor",
    "saved"
]


class Market():
    """ Market data container """

    def __init__(self, name: str,
                 pairs: List[CryptoPair],
                 retirement=Retirement,
                 association=Association):

        self.ohlcv: Dict[str, DataFrame] = None
        self.ticker: Dict[str, DataFrame] = None
        self.name = name
        self.association = association(self, pairs)
        self.retirement = retirement(self)

        self.ohlcv = self.association()

    def add(self, pair: CryptoPair, entry, further_data: List=[]):
        """
        Add a new entry(pair='BTC/ETH', current=float, high, low, median)
        to the market container
        """
        if isinstance(entry, dict):
            self.add_ticker(pair=pair, entry=entry, further_data=further_data)
        elif isinstance(entry, list):
            self.add_ohlcv(pair=pair, entries=entry, further_data=further_data)

        self.retirement()

    def add_ticker(self, pair: CryptoPair, entry, further_data: List):
        """ Adds a ticker element to market data """
        pass

    def prepare_ohlcv_entries(self,
                              pair: CryptoPair,
                              entries,
                              further_data: List=[]):

        """ Prepares the ohlcv entries for addition to the dataframe """
        entries = [[*entry, pair(), self.name, *further_data, False]
                   for entry in entries]
        return entries

    def add_ohlcv(self, pair: CryptoPair, entries, further_data: List=[]):
        """ Adds a ohlcv element to market data """

        tmp = DataFrame(self.prepare_ohlcv_entries(pair,
                                                   entries,
                                                   further_data),
                        columns=OHLCV_COLUMN_NAMES)

        if self.ohlcv is None:
            self.ohlcv = {pair(): tmp}
        elif self.ohlcv[pair()] is None:
            self.ohlcv[pair()] = tmp
        else:
            self.ohlcv[pair()] = self.ohlcv[pair()]\
                .append(tmp, ignore_index=True)\
                .drop_duplicates(subset='Timestamp')

    def plot_pair(self, pair, time_frame=None):
        """ Plots a pair with an optional time frame """
        # TODO implement me
        pass

    def print_table(self, pair: CryptoPair):
        """ Prints a table of the current market """
        print(self.ohlcv[pair()])

    def get_last_fetch_timestamp(self, pair: CryptoPair):
        """ Returns the last fetched timestamp in milliseconds """
        start_timestamp = 1_451_606_400_000
        if self.ohlcv is None:
            return start_timestamp
        elif self.ohlcv[pair()] is None:
            return start_timestamp
        elif len(self.ohlcv[pair()]) == 0:
            return start_timestamp
        else:
            return self.ohlcv[pair()].Timestamp.max()

    def get_market_data(self):
        """ Gets the currently used data container """
        if self.ticker is None:
            return self.ohlcv
        else:
            return self.ticker
