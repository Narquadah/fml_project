"""
Represents a currency/cryptocurrency
"""

from decimal import Decimal


class Currency():
    """
    Represents a currency/cryptocurrency
    """

    name: str
    symbol: str
    in_dollar: Decimal
    in_euro: Decimal
    in_btc: Decimal
    in_eth: Decimal

    def __init__(self, name: str, symbol: str):
        self.name = name
        self.symbol = symbol

    def calculate_values(self):
        """ Sets the in_* values """
        pass

    @staticmethod
    def from_symbol(symbol: str):
        """ Instantiates Currency from symbol """
        # TODO implement
        return Currency(name="tmp", symbol=symbol)

    @staticmethod
    def from_name(name: str):
        """ Instantiates Currency from name """
        # TODO implement
        return Currency(name=name, symbol="tmp")


class CryptoPair():
    """ A crypto currency pair (ex: 'ETH/BTC') """

    base: Currency
    quote: Currency

    def __init__(self, pair: str):
        self.pair = pair
        self.check_validity()
        tmp_pair = pair.split('/')
        # self.base = Currency.from_symbol(tmp_pair[0])
        # self.quote = Currency.from_symbol(tmp_pair[1])
        self.base = tmp_pair[0]
        self.quote = tmp_pair[1]

    def check_validity(self):
        """ Checks whether the pair is valid """
        if self.pair.find('/') == -1:
            raise ValueError("A CryptoPair must contain a '/'")

    def __str__(self):
        return self.pair

    def __call__(self):
        return self.pair

    def escape(self):
        return self.pair.replace('/', '-')
