import json
import os
import pickle
from datetime import datetime

import numpy as np
from keras.callbacks import (CSVLogger, EarlyStopping, ModelCheckpoint,
                             TensorBoard)
from keras.models import Sequential, load_model
from tqdm import tqdm

from crypto import CryptoPair
from ms import Data, networks

SELECTED_MODEL = 'lstm_2'


def main():
    np.random.seed(42)
    with open('./config.json') as json_data:
        config = json.load(json_data)

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    pair = poloniex_pairs[0]

    # crypto_name = '{}-USD'.format(pair.base)
    crypto_name = pair.escape()

    data = Data(interval='percentage_positive_15min',
                crypto=pair,
                window_size=100,
                name=crypto_name,
                batch_size=32,
                label_size=1,
                enable_scaler=False)

    training_labels, training_features = data.training()
    validation_labels, validation_features = data.validation()

    for net in tqdm(networks):
        if net.__name__ != SELECTED_MODEL:
            continue

        print('___________________________')
        print('Training {}'.format(net.__name__))
        print('___________________________')

        path = './models/{}/'.format(net.__name__)
        tensor_dir = path + 'tensorboard'
        checkpoint_dir = path + 'checkpoints/'

        filename = path + '{}-15min_{}.model'\
            .format(crypto_name, net.__name__)

        history_filename = path + '{}_{}.history'\
            .format(datetime.now().strftime('%y-%m-%d_%H-%M-%S'), net.__name__)

        csv_filename = path + '{}_{}.csv'\
            .format(datetime.now().strftime('%y-%m-%d_%H-%M-%S'), net.__name__)

        checkpoint_filename = checkpoint_dir + net.__name__ +\
            '-{epoch:02d}-{val_loss:.5f}.model'

        for p in [path, tensor_dir, checkpoint_dir]:
            if not os.path.exists(p):
                os.makedirs(p)

        model: Sequential
        if os.path.exists(filename) and os.path.isfile(filename):
            model: Sequential = load_model(filename)
        else:
            model: Sequential = net.build_model(training_features)

        csv_logger = CSVLogger(csv_filename, append=True)
        tensor_board = TensorBoard(
            log_dir=tensor_dir,
            histogram_freq=1,
            write_grads=True,
            write_graph=True,
            write_images=True,
            batch_size=net.batch_size
        )

        model_checkpoint = ModelCheckpoint(checkpoint_filename,
                                           monitor='val_loss',
                                           verbose=1,
                                           mode='min')

        model.fit(x=training_features, y=training_labels,
                  batch_size=net.batch_size,
                  validation_data=(validation_features, validation_labels),
                  epochs=net.epochs,
                  shuffle=False,
                  callbacks=[csv_logger,
                             model_checkpoint,
                             tensor_board])

        model.save(filename)
        with open(history_filename, 'wb') as file:
            pickle.dump(res.history, file)


main()
