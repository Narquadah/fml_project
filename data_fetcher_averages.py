""" Main file to start the file, entrypoint! """

from crypto.average_prices import AveragePrices


def main():
    """ Starts the main program and handles the cli api """
    avg = AveragePrices()
    avg.run()

main()
