import os
import pickle

from tqdm import tqdm
from pandas import DataFrame, read_csv

import matplotlib.pyplot as plt


from crypto import CryptoPair


def plot_history(history, filename):
    basepath = os.path.dirname(filename)

    history = history[['loss', 'val_loss']]

    plt.figure()
    history.plot(logy=True)
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.savefig('{}/{}_loss_epoch.png'.format(basepath,
                                              basepath.split('/')[-1]))
    plt.close()


def main():
    files = os.popen('find models -type f -name "*.csv"')\
        .read()\
        .split('\n')[0:-1]

    for history_filename in tqdm(files):
        history = read_csv(history_filename)
        plot_history(history, history_filename)


main()
