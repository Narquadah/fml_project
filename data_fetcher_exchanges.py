""" Main file to start the file, entrypoint! """
# import logging
# logging.basicConfig(level=logging.DEBUG)

import asyncio
import json

from crypto.currency import CryptoPair
from crypto.exchange import Binance, Poloniex


def main():
    """ Starts the main program and handles the cli api """
    with open('./config.json') as json_data:
        config = json.load(json_data)
        json_data.close()

    binance_pairs = []
    poloniex_pairs = []

    for exchange in config['trading_pairs']:
        for raw_pair in config['trading_pairs'][exchange]:
            if exchange == 'binance':
                binance_pairs.append(CryptoPair(raw_pair))
            elif exchange == 'poloniex':
                poloniex_pairs.append(CryptoPair(raw_pair))

    binance = Binance(cryptos=binance_pairs)
    poloniex = Poloniex(cryptos=poloniex_pairs)

    asyncio.get_event_loop().run_until_complete(poloniex.catch_up())
    asyncio.get_event_loop().run_until_complete(binance.catch_up())


main()
