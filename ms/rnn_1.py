import keras
from keras.models import Sequential, load_model
from keras.layers import Activation, Dense
from keras.layers import RNN
from keras.layers import Dropout
from keras import losses

import numpy as np
from tqdm import tqdm

from crypto import CryptoPair
from .data import Data


class rnn_1():

    window_size = 100
    batch_size = 1024
    epochs = 10

    @staticmethod
    def build_model(features):
        model = Sequential(name='rnn_1')
        model.add(RNN(1000,
                      return_sequences=True,
                      input_shape=(features.shape[1], features.shape[2]),
                      activation='tanh'))

        model.add(Dropout(0.1))
        model.add(RNN(250, return_sequences=True, activation='tanh'))
        model.add(Dropout(0.1))
        model.add(RNN(250, activation='tanh'))
        model.add(Dropout(0.1))
        model.add(Dense(units=1))
        model.add(Activation('tanh'))
        model.compile(loss=losses.logcosh,
                      optimizer='adam', metrics=['accuracy'])

        model.summary()
        return model
