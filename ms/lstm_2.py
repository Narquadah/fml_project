import keras
from keras.models import Sequential, load_model
from keras.layers import Activation, Dense, Dropout, LSTM, LeakyReLU

import numpy as np
from tqdm import tqdm

from crypto import CryptoPair
from .data import Data


class lstm_2():

    window_size = 100
    batch_size = 32
    epochs = 100

    @staticmethod
    def build_model(features):
        model = Sequential(name='lstm_2')
        model.add(LSTM(128,
                       return_sequences=True,
                       input_shape=(features.shape[1], features.shape[2]),
                       activation='tanh'))

        model.add(Dropout(0.8))

        # model.add(LSTM(128, return_sequences=True, activation='tanh'))
        # model.add(Dropout(0.80))

        model.add(LSTM(128, activation='tanh'))
        model.add(Dropout(0.80))

        model.add(Dense(units=1))
        model.add(LeakyReLU())

        model.compile(loss='mae', optimizer='adam')
        model.summary()
        return model
