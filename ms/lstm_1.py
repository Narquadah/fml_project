import keras
from keras.models import Sequential, load_model
from keras.layers import Activation, Dense
from keras.layers import LSTM, Dropout, LeakyReLU, CuDNNLSTM

import numpy as np
from tqdm import tqdm

from crypto import CryptoPair
from .data import Data


class lstm_1():

    window_size = 100
    batch_size = 16
    epochs = 100

    @staticmethod
    def build_model(features):
        model = Sequential(name='lstm_1')
        model.add(LSTM(units=64,
                       activity_regularizer=keras.regularizers.l1(1),
                       input_shape=(features.shape[1], features.shape[2]),
                       return_sequences=False))
        model.add(Activation('tanh'))
        model.add(Dropout(0.2))

        # model.add(LSTM(64, activation='tanh'))
        # model.add(Dropout(0.20))

        model.add(Dense(1))
        model.add(LeakyReLU())

        model.compile(loss='mse', optimizer='adam')
        model.summary()
        return model
