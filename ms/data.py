"""
"""
import h5py as h5
import numpy as np
import math

from sklearn.preprocessing import MinMaxScaler

from crypto import CryptoPair


class Data():
    """
    """

    training_features: np.array
    validation_features: np.array
    training_labels: np.array
    validation_labels: np.array

    def __init__(self,
                 interval: str,
                 crypto: CryptoPair,
                 window_size: int,
                 prefix='',
                 name='',
                 batch_size=1,
                 label_size=1,
                 filename='ms',
                 min_max_feature_range=(-1, 1),
                 enable_scaler=True):

        self.enable_scaler = enable_scaler
        self.crypto = crypto()
        self.batch_size = batch_size
        self.scaler = MinMaxScaler(feature_range=min_max_feature_range)
        self.label_size = label_size
        path = 'db/{}{}_data_{}.h5'.format(prefix, filename, interval)
        with h5.File(path, mode='r') as file:
            data = np.array(file[name]['features'])
            self.prepare(data, window_size)

    def prepare(self, data, window_size):
        data_length = len(data)

        if self.enable_scaler:
            data = data.reshape(-1, 1)
            data = self.scaler.fit_transform(data)
            data = np.squeeze(data)
        self.data = data

        data_training = data[:math.floor(data_length * 0.8)]
        data_validation = data[math.floor(data_length * 0.8):data_length]

        self.training_features, self.training_labels = self.sliding_window(
            data_training,
            window_size)

        self.validation_features, self.validation_labels = self.sliding_window(
            data_validation,
            window_size)

    def sliding_window(self, data, window_size):
        features = []
        labels = []
        for start in range(len(data) - window_size):
            current_window = data[start: start + window_size]
            features.append(current_window)
            labels.extend(data[start + window_size:
                               start + window_size + self.label_size])
        features = np.array(features).reshape((*np.shape(features), 1))

        if self.label_size != 1:
            label_dims = (math.floor(len(labels) / self.label_size),
                          self.label_size)
            labels = np.array(labels)[:-(len(labels) % self.label_size)]\
                .reshape(label_dims)

        return features, labels

    def training(self):
        len_labels = len(self.training_labels)
        max_amount = len_labels - (len_labels % self.batch_size)
        return self.training_labels[:max_amount], self.training_features[:max_amount]

    def validation(self):
        len_labels = len(self.validation_labels)
        max_amount = len_labels - (len_labels % self.batch_size)
        return self.validation_labels[:max_amount], self.validation_features[:max_amount]


# class DataCurrent()
