import keras
from keras.models import Sequential, load_model
from keras.layers import Activation, Dense
from keras.layers import LSTM
from keras.layers import Dropout

import numpy as np
from tqdm import tqdm

from crypto import CryptoPair
from .data import Data


class lstm_3():

    window_size = 100
    batch_size = 1024
    epochs = 10

    @staticmethod
    def build_model(features):
        model = Sequential(name='lstm_3')
        model.add(LSTM(128,
                       return_sequences=True,
                       input_shape=(features.shape[1], features.shape[2]),
                       activation='tanh'))

        model.add(LSTM(256, return_sequences=True, activation='tanh'))
        model.add(LSTM(256, activation='tanh'))

        model.add(Dense(units=1))
        model.add(Activation('tanh'))
        model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
        model.summary()
        return model
