import keras
from keras.models import Sequential, load_model
from keras.layers import Activation, Dense, LSTM, Dropout
import numpy as np
from tqdm import tqdm

from crypto import CryptoPair
from .data import Data


class lstm_cat():

    window_size = 100
    batch_size = 1024
    epochs = 10

    @staticmethod
    def build_model(features):
        model = Sequential(name='lstm_cat')
        model.add(LSTM(1024,
                       return_sequences=True,
                       input_shape=(features.shape[1], features.shape[2]),
                       activation='relu',
                       dropout=0.25,
                       recurrent_dropout=0.25))

        model.add(LSTM(256,
                       return_sequences=True,
                       activation='relu',
                       dropout=0.25,
                       recurrent_dropout=0.25))

        model.add(LSTM(256,
                       activation='relu',
                       dropout=0.25,
                       recurrent_dropout=0.25))

        model.add(Dense(13, activation='softmax'))

        model.compile(loss='categorical_crossentropy',
                      optimizer='sgd', metrics=['accuracy'])

        model.summary()
        return model
