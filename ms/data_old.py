"""
"""
import h5py as h5
import numpy as np
import math

from crypto import CryptoPair


class Data():
    """
    """

    training_features: np.array
    test_features: np.array
    validation_features: np.array
    training_labels: np.array
    test_labels: np.array
    validation_labels: np.array

    def __init__(self,
                 interval: str,
                 crypto: CryptoPair,
                 window_size: int,
                 prefix='',
                 name=''):

        self.crypto = crypto()
        path = 'db/{}ms_data_{}.h5'.format(prefix, interval)
        with h5.File(path, mode='r') as file:
            data = np.array(file[name]['features'])
            self.prepare(data, window_size)

    def prepare(self, data, window_size):
        data_length = len(data)

        data_training = data[:math.floor(data_length * 0.7)]
        data_test = data[
            math.floor(data_length * 0.7):
            math.floor(data_length * 0.9)
        ]

        data_validation = data[math.floor(data_length * 0.9):data_length]

        self.training_features = self.make_features(
            data_training,
            window_size)

        self.test_features = self.make_features(
            data_test,
            window_size)

        self.validation_features = self.make_features(
            data_validation,
            window_size)

        self.training_labels = self.make_labels(
            data_training,
            window_size)

        self.test_labels = self.make_labels(
            data_test,
            window_size)

        self.validation_labels = self.make_labels(
            data_validation,
            window_size)

    def make_features(self, data, window_size):
        result = []
        for start in range(len(data) - window_size):
            current_window = data[start: start + window_size]
            result.append(current_window)
        result = np.array(result).reshape((*np.shape(result), 1))
        return result

    def make_labels(self, data, window_size):
        result = []
        for start in range(len(data) - window_size):
            current_window = data[start: start + window_size]
            result.append(self.normalize_label(current_window))
        return np.array(result)

    def normalize_label(self, data):
        state = sum(data)
        if state < 0:
            return -1
        elif state > 0:
            return 1
        return 0

    def training(self):
        return self.training_labels, self.training_features

    def test(self):
        return self.test_labels, self.test_features

    def validation(self):
        return self.validation_labels, self.validation_features


# class DataCurrent()